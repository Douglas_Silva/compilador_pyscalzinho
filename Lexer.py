﻿#-*- coding: utf-8 -*-

'''
Created on 07/02/2013

@authors: Douglas Henrique
          Lucas de Holanda
          Ricardo Dantas

Projeto Disciplina Compiladores

Compilador linguagem MicroPascal desenvolvido em Python
'''
from Token import Token
from TokenType import TokenType
from CompilerException import CompilerException 

#Dicionário para checar de o lexema encontrado é uma palavra reservada
keyWords = {        
        #Palavras reservadas
        'program': TokenType.PROGRAM,'if': TokenType.IF,'then': TokenType.THEN,'else': TokenType.ELSE,'while': TokenType.WHILE,
        'do': TokenType.DO,'repeat': TokenType.REPEAT,'until': TokenType.UNTIL,
        'integer': TokenType.INTEGER,'real': TokenType.REAL,'char': TokenType.CHAR,'begin': TokenType.BEGIN,
        'end': TokenType.END,'write': TokenType.WRITE,'var': TokenType.VAR,'div': TokenType.DIV,
        'and': TokenType.AND,'or': TokenType.OR,'not': TokenType.NOT
       }

class Lexer:
    '''
    Classe Lexer que instaciará um objeto lexer, sendo este
    utilizado no Parser para a criação dos objetos tokens.
    Ele é responsável pela etapa de analise Léxica.
    '''

    def __init__(self,targetArchive):
        '''
        Construtor
        '''
        
        self.targetArchive    = open(targetArchive, 'r')
        self.nextChar         = ' '
        self.numberLine       = 1  
    
    def readChar(self):
        '''
        Método que será utilizado pelo nextToken para atualização do atributo self.nextChar
        '''
        self.nextChar        = self.targetArchive.read(1)
        

                
    def nextToken(self):
        '''
        Método que será utilizado pelo Parser para criação dos objetos tokens.
        '''
        
        lexema                = ''
            
        if self.nextChar == ' ' or self.nextChar == '\t' or self.nextChar == '\r':
            self.readChar()
            while self.nextChar == ' ' or self.nextChar == '\t':
                self.readChar()
        
                
        if self.nextChar.isalpha() or self.nextChar == '_' :
            lexema           += self.nextChar
            self.readChar()
            
            while self.nextChar.isalpha() or self.nextChar.isdigit() or self.nextChar == '_':
                lexema           += self.nextChar
                self.readChar()                
            
            if keyWords.has_key(lexema):
                return Token(tokenType = keyWords[lexema], lexema = lexema, line = self.numberLine)
            else:
                return Token(tokenType = TokenType.IDENTIFICADOR, lexema = lexema , line = self.numberLine)
        
        
        elif self.nextChar.isdigit():
            lexema           += self.nextChar
            self.readChar()
            
            
            while self.nextChar.isdigit():
                lexema           += self.nextChar
                self.readChar()                
                
            if self.nextChar == '.':
                lexema           += self.nextChar
                self.readChar()
                
                while self.nextChar.isdigit():
                    lexema           += self.nextChar
                    self.readChar()  
                                      
                if lexema[-1]=='.':
                    # cai nesse if na seguinte condição '123.' , dai ele seta o self.nextChar
                    # com o ultimo caracter do lexema, pois o lexema deverá ser '123' 
                    # seguindo a seguinte ER digito*.digito+, e o nextChar da próxima chamada
                    # será novamente o '.'
                    self.nextChar    = lexema[-1] 
                    lexema           = lexema[:-1]
                     
                    return Token(tokenType = TokenType.REAL_LITERAL, lexema = lexema , line = self.numberLine)
                else:
                    return Token(tokenType = TokenType.REAL_LITERAL , lexema = lexema , line = self.numberLine)
            
            else:
                    return Token(tokenType = TokenType.INTEIRO_LITERAL, lexema = lexema , line = self.numberLine)

                    
        elif self.nextChar == '+':            
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.MAIS, lexema = lexema, line = self.numberLine)
                
        
        elif self.nextChar == '-':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.MENOS, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == '*':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.VEZES, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == '/':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.DIVIDIDO, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == '(':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.ABRE_PAR, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == ')':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.FECHA_PAR, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == ',':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.VIRGULA, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == ';':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.PT_VIRGULA, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == '.':
            lexema             += self.nextChar
            self.readChar()
            
            if self.nextChar.isdigit():
                lexema             += self.nextChar
                self.readChar()
                
                while self.nextChar.isdigit():
                    lexema           += self.nextChar
                    self.readChar()
                
                return Token(tokenType = TokenType.REAL_LITERAL, lexema = '0' + lexema , line = self.numberLine)
            
            else:     
                return Token(tokenType = TokenType.PONTO, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == '=':
            lexema             += self.nextChar
            self.readChar()
            return Token(tokenType = TokenType.IGUAL_A, lexema = lexema, line = self.numberLine)
            
        elif self.nextChar == ':':
            lexema           += self.nextChar
            self.readChar()
            if self.nextChar=='=':
                lexema           += self.nextChar
                self.readChar()
                return Token(tokenType = TokenType.ATRIBUICAO, lexema = lexema, line = self.numberLine)
            else:
                return Token(tokenType = TokenType.DOIS_PONTOS, lexema = lexema, line = self.numberLine)
                
                
        elif self.nextChar == '<':
            lexema           += self.nextChar
            self.readChar()
            
            if self.nextChar == '=':
                lexema           += self.nextChar
                self.readChar()
                return Token(tokenType = TokenType.MENORIGUAL_QUE, lexema = lexema, line = self.numberLine)
            
            elif self.nextChar == '>':
                lexema           += self.nextChar
                self.readChar()
                return Token(tokenType = TokenType.DIFERENTE_DE, lexema = lexema, line = self.numberLine)
            
            else:
                return Token(tokenType = TokenType.MENOR_QUE, lexema = lexema, line = self.numberLine)
            
        
        elif self.nextChar == '>':
            lexema           += self.nextChar
            self.readChar()
            
            if self.nextChar == '=':
                lexema           += self.nextChar
                self.readChar()
                return Token(tokenType = TokenType.MAIORIGUAL_QUE, lexema = lexema, line = self.numberLine)
            else:
                return Token(tokenType = TokenType.MAIOR_QUE, lexema = lexema, line = self.numberLine)
        
        
        elif self.nextChar == "'":
            lexema           += self.nextChar
            self.readChar()
            
            if self.nextChar.isalpha() or self.nextChar.isdigit() :
                
                lexema           += self.nextChar
                self.readChar()
                
                if self.nextChar == "'":
                    lexema           += self.nextChar
                    self.readChar()
                    return Token(tokenType = TokenType.CHARACTER_LITERAL, lexema = lexema, line = self.numberLine)
            
            elif self.nextChar == "\\"[0]:
                lexema           += self.nextChar
                self.readChar()
                
                if self.nextChar == 'n' or self.nextChar == 't':
                    lexema           += self.nextChar
                    self.readChar()
                    
                    if self.nextChar == "'":
                        lexema           += self.nextChar
                        self.readChar()
                        return Token(tokenType = TokenType.CHARACTER_LITERAL, lexema = lexema, line = self.numberLine)
                            
                            
        elif self.nextChar == '\n':
            self.numberLine       += 1
            self.readChar()
            return self.nextToken()
        
        
        elif self.nextChar == '':
            return Token(tokenType = TokenType.EOF)
        
        else:
            raise CompilerException("Caracter inesperado: '%s' linha: %d" % (self.nextChar,self.numberLine))
