#-*- coding: utf-8 -*-
'''
Created on 07/02/2013

@authors: Douglas Henrique
          Lucas de Holanda
          Ricardo Dantas

Projeto Disciplina Compiladores

Compilador linguagem MicroPascal desenvolvido em Python
'''
                     
class Token:
    '''
    Classe Token que instaciará os objetos token
    tendo como atributos o tipo do token, e o lexema
    '''
    

    def __init__(self,tokenType,lexema=None,line=None):
        '''
        Construtor
        '''
        
        self.tokenType      = tokenType
        self.lexema         = lexema
        self.line           = line
        
    def getTokenType(self):
        '''
        Retorna o tipo do Token.
        '''
        
        return self.tokenType
    
    def getLexema(self):
        '''
        Retorna o lexema do Token.
        '''
        
        return self.lexema
    
    def getLine(self):
        '''
        Retorna a linha do Token.
        '''
        
        return self.line
    
    def toString(self):
        '''
        Método de representação do objeto em forma de string.
        '''
        if not self.lexema:
            return "[" + str(self.tokenType) + "]"
        else:
            return "[" + str(self.tokenType) + "," + self.lexema + ",Linha:" + str(self.line) + "]"