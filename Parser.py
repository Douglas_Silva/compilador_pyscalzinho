﻿#-*- coding: utf-8 -*-
'''
Created on 16/02/2013

@authors: Douglas Henrique
          Lucas de Holanda
          Ricardo Dantas

Projeto Disciplina Compiladores

Compilador linguagem MicroPascal desenvolvido em Python
'''
from Lexer import Lexer
from CompilerException import CompilerException
from TokenType import TokenType

class Parser:
    '''
    classe que será responsável pela etapa de análise
    sintático no compilador
    
      
    IDENTIFICADOR  0 , MENOR_QUE  1 , MAIOR_QUE  2 , MENORIGUAL_QUE  3,
    MAIORIGUAL_QUE  4, IGUAL_A  5, DIFERENTE_DE  6, MAIS  7, MENOS  8, 
    VEZES  9, DIVIDIDO  10, DIV  11, AND  12, 
    OR  13, NOT  14, ATRIBUICAO  15, ABRE_PAR  16, 
    FECHA_PAR  17, VIRGULA  18, PT_VIRGULA  19, PONTO  20, 
    DOIS_PONTOS  21, PROGRAM  22, IF  23, THEN  24, 
    ELSE  25, WHILE  26, DO  27, REPEAT  28, 
    UNTIL  29, INTEGER  30, REAL  31, CHAR  32, 
    BEGIN  33, END  34, WRITE  35, VAR  36,
    INTEIRO_LITERAL  37, REAL_LITERAL  38, CHARACTER_LITERAL  39, EOF  40)
    '''

    def __init__(self,targetArchive):
        '''
        Construtor
        '''
        self.lexer          = Lexer(targetArchive)
        self.currentToken   = ''
        
    
    def parse (self):
        self.currentToken   = self.lexer.nextToken()
        self.parseProgram()
        self.acceptToken(TokenType.EOF)
        print 'Analise léxica concluída. Codigo fonte aceito!'
        return True
        
    def acceptToken(self,tokenType=None):
        if tokenType:
            
            if self.currentToken.getTokenType() == tokenType:
                self.currentToken = self.lexer.nextToken()
            
            else:
                raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))
        
        else:
            self.currentToken = self.lexer.nextToken()        


    def parseProgram(self):
        '''
            <program> ::= program IDENTIFICADOR ; <secao_var> <bloco>.
        '''
        self.acceptToken(TokenType.PROGRAM)
        self.acceptToken(TokenType.IDENTIFICADOR)
        self.acceptToken(TokenType.PT_VIRGULA )
        self.parseSecao_Var()
        self.parseBloco()
        self.acceptToken(TokenType.PONTO)
        
    
    def parseSecao_Var(self):
        '''
            <secao_var> :: = var (<decl_var>)*
        '''
        self.acceptToken(TokenType.VAR) 
        
        if self.currentToken.getTokenType() == TokenType.IDENTIFICADOR:
            while self.currentToken.getTokenType() == TokenType.IDENTIFICADOR:
                self.parseDecl_Var()
        
        elif self.currentToken.getTokenType() == TokenType.BEGIN:
            pass

        else:
            raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))
            
            
    def parseDecl_Var(self):
        '''
            <decl_var> ::= IDENTIFICADOR (, IDENTIFICADOR)* : <tipo> ; 
        '''
        self.acceptToken(TokenType.IDENTIFICADOR)
        
        if self.currentToken.getTokenType() == TokenType.VIRGULA:
            self.acceptToken()
            self.acceptToken(TokenType.IDENTIFICADOR)
           
            while self.currentToken.getTokenType() == TokenType.VIRGULA:
                self.acceptToken()
                self.acceptToken(TokenType.IDENTIFICADOR)
            
            if self.currentToken.getTokenType() == TokenType.DOIS_PONTOS:
                self.acceptToken()
            
            else:
                raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))
            
            self.parseTipo()
            self.acceptToken(TokenType.PT_VIRGULA)
            
        elif self.currentToken.getTokenType() == TokenType.DOIS_PONTOS:
            self.acceptToken()
            self.parseTipo()
            self.acceptToken(TokenType.PT_VIRGULA)
        
        else:
            raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))
            
            
    def parseTipo(self):
        '''
            <tipo> ::= “integer” | “real” | “char”
        '''
        if self.currentToken.getTokenType() in (TokenType.INTEGER,TokenType.REAL,TokenType.CHAR):
            self.acceptToken()
        
        else:
            raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))


    def parseBloco(self):
        '''
            <bloco> ::= begin <lista_comandos> end
        '''
        self.acceptToken(TokenType.BEGIN)
        self.parseListaComandos()
        self.acceptToken(TokenType.END)
        
        
    def parseListaComandos(self):
        '''
            <lista_comandos> ::= (<comandos>)*

        '''
        if self.currentToken.getTokenType() == TokenType.END:
            pass
        
        elif self.currentToken.getTokenType() in (TokenType.BEGIN, TokenType.IDENTIFICADOR, TokenType.WHILE, 
                                                  TokenType.REPEAT, TokenType.IF, TokenType.WRITE):
            
            while self.currentToken.getTokenType() in (TokenType.BEGIN, TokenType.IDENTIFICADOR, TokenType.WHILE, 
                                                  TokenType.REPEAT, TokenType.IF, TokenType.WRITE):
                self.parseComando()
        
        else:
            raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))  
        
        
    def parseComando(self):
        '''
            <comando> ::= <bloco>;
                          |<atribuição>
                          |<iteracao>
                          |<decisao>
                          |<escrita>
        '''
        
        if self.currentToken.getTokenType() == TokenType.BEGIN:
            self.parseBloco()
            self.acceptToken(TokenType.PT_VIRGULA)
            
        elif self.currentToken.getTokenType() == TokenType.IDENTIFICADOR:
            self.parseAtribuicao()
            
        elif self.currentToken.getTokenType() in (TokenType.WHILE,TokenType.REPEAT):
            self.parseIteracao()
        
        elif self.currentToken.getTokenType() == TokenType.IF:
            self.parseDecisao()
            
        elif self.currentToken.getTokenType() == TokenType.WRITE:
            self.parseEscrita()
            
        else:
            raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))
    
    
    def parseAtribuicao(self):
        '''
            <atribuicao> ::= IDENTIFICADOR “:=” <expressao> ;
        '''
        self.acceptToken(TokenType.IDENTIFICADOR)
        self.acceptToken(TokenType.ATRIBUICAO) 
        self.parseExpressao()
        self.acceptToken(TokenType.PT_VIRGULA)           
    
    
    def parseIteracao(self):
        '''
            <iteracao> ::=  while <expressao> do <comando> 
                          | repeat <comando> until <expressao> ;

        '''
        if self.currentToken.getTokenType() == TokenType.WHILE:
            self.acceptToken()
            self.parseExpressao()
            self.acceptToken(TokenType.DO)
            self.parseComando()
        
        elif self.currentToken.getTokenType() == TokenType.REPEAT:
            self.acceptToken()
            self.parseComando()
            self.acceptToken(TokenType.UNTIL)
            self.parseExpressao()
            self.acceptToken(TokenType.PT_VIRGULA)
                
        else:
            raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))
    
    
    def parseDecisao(self):
        '''
            <decisao> ::= if <expressao> then <comando> <restoDecisao>
        '''
        self.acceptToken(TokenType.IF)
        self.parseExpressao()
        self.acceptToken(TokenType.THEN)
        self.parseComando()
        self.parseRestoDecisao()
                         
    
    def parseRestoDecisao(self):
        '''
            <restoDecisão> ::=   else <comando>
                               | vazio
        '''
        if self.currentToken.getTokenType() == TokenType.ELSE:
            self.acceptToken()
            self.parseComando
        
        else:
            pass
            
        
    def parseEscrita(self):
        '''
            <escrita> ::= write “(” <expressao> “)” ;
        '''
        self.acceptToken(TokenType.WRITE)
        self.acceptToken(TokenType.ABRE_PAR)
        self.parseExpressao()
        self.acceptToken(TokenType.FECHA_PAR)
        self.acceptToken(TokenType.PT_VIRGULA)
        
                            
    def parseExpressao(self):
        '''
            <expressao> ::= <ExpressaoB> <ExpressaoCont>
        '''
        self.parseExpressaoB()
        self.parseExpressaoCont()
        
    
    
    def parseExpressaoCont(self):
        '''
            <ExpressaoCont> ::=  + <ExpressaoB> <ExpressaoCont>
                               | - <ExpressaoB> <ExpressaoCont> 
                               | vazio
        '''
        if self.currentToken.getTokenType() == TokenType.MAIS:
            self.acceptToken()
            self.parseExpressaoB()
            self.parseExpressaoCont()
        
        elif self.currentToken.getTokenType() == TokenType.MENOS:
            self.acceptToken()
            self.parseExpressaoB()
            self.parseExpressaoCont()
        
        else:
            pass
    
    
    def parseExpressaoB(self):
        '''
            <ExpressaoB> ::= <ExpressaoC> <ExpressaoBCont>
        '''
        self.parseExpressaoC()
        self.parseExpressaoBCont()
        
    
    def parseExpressaoBCont(self):
        '''
            <ExpressaoBCont> ::=   * <ExpressaoC> <ExpressaoBCont>
                                 | / <ExpressaoC> <ExpressaoBCont>
                                 | "DIV" <ExpressaoC> <ExpressaoBCont>
                                 | vazio
        '''
        if self.currentToken.getTokenType() == TokenType.VEZES:
            self.acceptToken()
            self.parseExpressaoC()
            self.parseExpressaoBCont()
        
        elif self.currentToken.getTokenType() == TokenType.DIVIDIDO:
            self.acceptToken()
            self.parseExpressaoC()
            self.parseExpressaoBCont()
        
        elif self.currentToken.getTokenType() == TokenType.DIV:
            self.acceptToken()
            self.parseExpressaoC()
            self.parseExpressaoBCont()
        
        else:
            pass
     
     
    def parseExpressaoC(self):  
        '''
            <ExpressaoC> ::= <ExpressaoBasica> <ExpressaoCCont>
        '''  
        self.parseExpressaoBasica()
        self.parseExpressaoCCont()
    
    
    def parseExpressaoCCont(self):
        '''
            <ExpressaoCCont> ::=   "and" <ExpressaoBasica> <ExpressaoCCont>
                                  | "or" <ExpressaoBasica> <ExpressaoCCont>
                                  | "="  <ExpressaoBasica> <ExpressaoCCont>
                                  | "<>" <ExpressaoBasica> <ExpressaoCCont>
                                  | "<=" <ExpressaoBasica> <ExpressaoCCont>
                                  | "<"  <ExpressaoBasica> <ExpressaoCCont>
                                  | ">=" <ExpressaoBasica> <ExpressaoCCont>
                                  | ">"  <ExpressaoBasica> <ExpressaoCCont>
                                  | vazio
        '''
        if self.currentToken.getTokenType() == TokenType.AND:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()    
        
        elif self.currentToken.getTokenType() == TokenType.OR:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()
        
        elif self.currentToken.getTokenType() == TokenType.IGUAL_A:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()
        
        elif self.currentToken.getTokenType() == TokenType.DIFERENTE_DE:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()
        
        elif self.currentToken.getTokenType() == TokenType.MENORIGUAL_QUE:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()
        
        elif self.currentToken.getTokenType() == TokenType.MENOR_QUE:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()
        
        elif self.currentToken.getTokenType() == TokenType.MAIORIGUAL_QUE:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()
            
        elif self.currentToken.getTokenType() == TokenType.MAIOR_QUE:
            self.acceptToken()
            self.parseExpressaoBasica()
            self.parseExpressaoCCont()
        
        else:
            pass
        
    
    def parseExpressaoBasica(self):
        '''
            <ExpressaoBasica> ::=  “(” <expressao> “)”
                                 | “not” <expressao>
                                 | INTEIRO_LITERAL
                                 | REAL_LITERAL
                                 | CHAR_LITERAL
                                 | IDENTIFICADOR
        '''
        if self.currentToken.getTokenType() == TokenType.ABRE_PAR:
            self.acceptToken()
            self.parseExpressao()
            self.acceptToken(TokenType.FECHA_PAR)
        
        elif self.currentToken.getTokenType() == TokenType.NOT:
            self.acceptToken()
            self.parseExpressao()
        
        elif self.currentToken.getTokenType() == TokenType.INTEIRO_LITERAL:
            self.acceptToken()
        
        elif self.currentToken.getTokenType() == TokenType.REAL_LITERAL:
            self.acceptToken()
        
        elif self.currentToken.getTokenType() == TokenType.CHARACTER_LITERAL:
            self.acceptToken()
            
        elif self.currentToken.getTokenType() == TokenType.IDENTIFICADOR:
            self.acceptToken()
            
        else:
            raise CompilerException("Erro de Sintaxe! Token: %s  Linha: %d" % (self.currentToken.lexema,self.currentToken.line))
        
        
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
