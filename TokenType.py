#-*- coding: utf-8 -*-
'''
Created on 07/02/2013

@authors: Douglas Henrique
          Lucas de Holanda
          Ricardo Dantas

Projeto Disciplina Compiladores

Compilador linguagem MicroPascal desenvolvido em Python
'''

class TokenType:
    '''classe "ENUM" que será utilizada para tipagem dos tokens.
      
    IDENTIFICADOR  0 , MENOR_QUE  1 , MAIOR_QUE  2 , MENORIGUAL_QUE  3,
    MAIORIGUAL_QUE  4, IGUAL_A  5, DIFERENTE_DE  6, MAIS  7, MENOS  8, 
    VEZES  9, DIVIDIDO  10, DIV  11, AND  12, 
    OR  13, NOT  14, ATRIBUICAO  15, ABRE_PAR  16, 
    FECHA_PAR  17, VIRGULA  18, PT_VIRGULA  19, PONTO  20, 
    DOIS_PONTOS  21, PROGRAM  22, IF  23, THEN  24, 
    ELSE  25, WHILE  26, DO  27, REPEAT  28, 
    UNTIL  29, INTEGER  30, REAL  31, CHAR  32, 
    BEGIN  33, END  34, WRITE  35, VAR  36,
    INTEIRO_LITERAL  37, REAL_LITERAL  38, CHARACTER_LITERAL  39, EOF  40)
    '''
    (IDENTIFICADOR, MENOR_QUE, MAIOR_QUE, MENORIGUAL_QUE,
    MAIORIGUAL_QUE, IGUAL_A, DIFERENTE_DE, MAIS, MENOS, 
    VEZES, DIVIDIDO, DIV, AND, 
    OR, NOT, ATRIBUICAO, ABRE_PAR, 
    FECHA_PAR, VIRGULA, PT_VIRGULA, PONTO, 
    DOIS_PONTOS, PROGRAM, IF, THEN, 
    ELSE, WHILE, DO, REPEAT, 
    UNTIL, INTEGER, REAL, CHAR, 
    BEGIN, END, WRITE, VAR,
    INTEIRO_LITERAL, REAL_LITERAL, CHARACTER_LITERAL, EOF) = range(0,41)
    
    
