﻿#-*- coding: utf-8 -*-
'''
Created on 17/02/2013

@authors: Douglas Henrique
          Lucas de Holanda
          Ricardo Dantas

Projeto Disciplina Compiladores

Compilador linguagem MicroPascal desenvolvido em Python
'''
from exceptions import Exception

class CompilerException(Exception):
    pass
